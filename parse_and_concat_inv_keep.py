# -*- coding: utf-8 -*-
"""
Created on Tue Jan 20 12:55:26 2015
@author: CHEN
"""
	# =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= # provide functions to parse and concatenate a blastn output file with the help 
	# of the fasta file used by blastn as database i.e. the reference contigs
	#
	# Designed for (gapped) blastn output format "6 evalue qstart qend sstart send qseqid 
	# sseqid qseq sseq", one species as ref and all the others as query. The 
	# opposite may cause bad alignments.
	#
	#
	#
	#
	#					#-------------------------------------------------#
	#					#  Wentao CHEN M2 BEE-r UJF       #
	#                   #  Thomas GOEURY ex-M2 BEE-r UJF  #
	#					#                    22 Jan 2015		        #
	#					# Python 2.7				       #
	#					#------------------------------------------------#
	#
	#
	# =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= #
	
	
import os
import sys
import Bio.Seq as bs
import copy
import commands
import re
from multiprocessing import Pool


# =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= #
#					CLASSES						#
# =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= #

###cProfile.run allows to see the calling profile

class blastn_result:
	'''Classe permettant de stocker des résultats d'un blast sous forme texte.
	
	L'objet permet de contenir la eValue, ainsi que pour la query et le subject : leur début, fin, nom et séquence'''
	def __init__(self,eV=0.0, qStart=0, qEnd=0, qName='',qSeq='', sStart=0, sEnd=0, sName='',sSeq='', rcSign=False):
	# eValue
		self.eValue=eV
	# Query
		self.qStart=qStart
		self.qEnd=qEnd
		self.qName=qName
		self.qSeq=qSeq
	# Subject
		self.sStart=sStart
		self.sEnd=sEnd
		self.sName=sName
		self.sSeq=sSeq
		self.rcSign=rcSign



# =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= #
#				FUNCTIONS						#
# =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= #

name_length = 6
notDNA = False
qSeq_path = ''
#consensus_threshold=1.0
#clustal_path='clustal-omega-1.2.0-win32/'

def loadRef(path):
	'''load the reference fasta and parse it, return a dict'''
	handle=open(path,'r')
	lines=handle.readlines()
	handle.close()
	ref_dict={}
	line = ''
	key = 'null'
	lines_size = len(lines)
	for i in range(0, lines_size):
		if lines[i][0] == '>' or lines[i] == '':
			ref_dict.update({key: line})
			key = lines[i][1:-1]
			key = key.replace(' ', '')
			line = ''
			continue
		else:
			line += lines[i][:-1]
	ref_dict.update({key: line})
	try: ref_dict.pop('null')
	except:
		0
#		if lines[i][0]=='>' and lines[i+1][0]!='>':
#			ref_dict.update({lines[i][1:-1]: lines[i+1][:-1]}) #-2 because in my assembly there is always a blank before \n
	return ref_dict
			
def loadBlast(path):
	'''Read a blastn output in format specified by -outfmt "6 evalue qstart qend sstart send qseqid sseqid qseq sseq"'''
	fichier=open(path,'r')
	lignes=fichier.readlines()
	fichier.close()
	return lignes
	
def retrieveInfo(ligne, notDNA = False, qSeqs = {}, sSeqs = {}):
	'''Au sein d'un résultat de blast tel que renvoyé par loadBlast(), permet de stocker dans un objet les informations contenus dans une ligne de ce résultat'''
	nom=blastn_result()
	if ligne[0]=='#': return None
	info=ligne.split('\t')
#	nom.eValue, nom.qStart, nom.qEnd, nom.sStart, nom.sEnd, nom.qName, nom.sName,nom.qSeq, nom.sSeq = float(info[0]), int(info[1]), int(info[2]), int(info[3]), int(info[4]), info[5], info[6], info[7], info[8][:-1]
	if not notDNA:
		nom.eValue, nom.qStart, nom.qEnd, nom.sStart, nom.sEnd, nom.qName, nom.sName,nom.qSeq, nom.sSeq = float(info[0]), int(info[1]), int(info[2]), int(info[3]), int(info[4]), info[5], info[6], info[7], info[8][:-1]
		if nom.sStart >  nom.sEnd or nom.qStart > nom.qEnd:
			nom.rcSign=True
	else:
		nom.eValue, nom.qStart, nom.qEnd, nom.sStart, nom.sEnd, nom.qName, nom.sName = float(info[0]), int(info[1]), int(info[2]), int(info[3]), int(info[4]), info[5], info[6]
		if nom.sStart > nom.sEnd:
			nom.sStart, nom.sEnd = nom.sEnd, nom.sStart
		if nom.qStart > nom.qEnd:
			nom.qStart, nom.qEnd = nom.qEnd, nom.qStart
		nom.qSeq = readDNAseq(qSeqs, nom.qName, nom.qStart, nom.qEnd)
		nom.sSeq = readDNAseq(sSeqs, nom.sName, nom.sStart, nom.sEnd)
	return nom

def readDNAseq(qSeqs, qName, qStart, qEnd):
	seq = qSeqs[qName]
	return seq[qStart-1:qEnd]
	
def storeInfos(lignes):
	'''Extrait l'information d'un fichier chargé en mémoire (arg: lignes), et le stocke dans une liste ou l'élément i de la liste correspond à la ligne i+1 du fichier'''
	qSeqs = {}
	if notDNA:
		qSeqs = loadRef(qSeq_path)
		sSeqs = ref_seq_list
	else:
		qSeqs = {}
		sSeqs = {}
		
	resultat = map(lambda ligne: retrieveInfo(ligne, notDNA, qSeqs, sSeqs), lignes) #[retrieveInfo(item) for item in lignes]
	return [item for item in resultat if item!=None]
	
#def getNames(resultat):
#	'''Recupère les noms uniques stockés dans resultat.qName'''
#	liste_name=[resultat[0].qName]
#	for element in resultat:
#		nom=element.qName
#		if nom!=liste_name[-1]:
#			liste_name.append(nom)
#	return liste_name

def getRefSeqNames(ListOfHits):
	'''Collect the sNames of the Reference sp'''
	Name_list=list(set(map(lambda item: item.sName, ListOfHits)))
	Name_list.sort()
	return Name_list

def getqSpeciesNames(list_of_hits):
	'''find all the species' names in the queries'''
	names=map(lambda item: item.qName[:name_length], list_of_hits)
	return list(set(names))

def findHitsByRefSeq(sName, ListOfHits):
	'''find all queries aligned to seq with sName, return a list of two elements'''
	return [sName, [obj for obj in ListOfHits if obj.sName==sName]]
	
def findqContigAligned(info_stock):
	'''return a list of species names and the corresponding contigs present in the dataset'''
	sp_list=list(set(map(lambda item: item.qName[:name_length], info_stock)))
	return [list(set([item.qName for item in info_stock if item.qName[:name_length]==name])) for name in sp_list]
	

def filterHits(list_of_hits, E_threshold):
	'''filter to be modified if you need'''
	return [item for item in list_of_hits if item.eValue <= E_threshold]
	
#def findRefseq(ref_hits, ref_dict):
#	'''ref_hits is a list from findHitsByRefSeq, return a ref seq'''	
#	ref_name=ref_hits[0]
#	hit_list=ref_hits[1]
##	se_list=[item.sStart for item in hit_list] + [item.sEnd for item in hit_list]
##	end_pos_ref=max(se_list)
##	start_pos_ref=min(se_list)
#	ref_seq=ref_dict[ref_name]
#	return ref_seq

def consensusW(word,threshold):
	'''
	A partir d'un mot de nucléotides (alphabet = {A, T, C, G, -}), renvoie la lettre correspondant au consensus selon les termes de l'IUPAC

		Argument :
			word -> les différents nucléotides dont on désire le consensus
			threshold -> le seuil, de 0 à 1, pour un consensus majoritaire :
							0 : Seul la lettre majoritaire est prise en compte
							1: Consensus total (toute les lettres présentes prises en compte)

		Renvoie :
			Une lettre, correspondant au consensus

	*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	*					IUPAC
	*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	*  1 letter consensus:
	*  A->A;	T->T;		C->C;	G->G
	*
	*  2 letters consensus:
	*  A&T->W;	A&C->M;	A&G->R
	*  T&C->Y;	T&G->K;	C&G->S
	*
	* 3 letters consensus:
	*  not A -> B
	*  not C -> D
	*  not T -> V
	*  not G -> H
	*
	*  4 letters consensus : N
	*
	* source: http://en.wikipedia.org/wiki/Nucleic_acid_notation
	*##################################
	'''
	nA=0; nT=0; nC=0; nG=0; n6=0
	# Comptage des nucléotides dans le mot.
	for letter in word: 
		if letter=='A':
			nA+=1.0
		if letter=='T':
			nT+=1.0
		if letter=='C':
			nC+=1.0
		if letter=='G':
			nG+=1.0
		if letter=='-':
			n6+=1.0
	somme=nA+nT+nC+nG+n6
	if somme==0:
		return 'N'
	# Proportions
	nA=float(nA/somme)
	nT=float(nT/somme)
	nC=float(nC/somme)
	nG=float(nG/somme)
	n6=float(n6/somme)
	liste=[('A',nA),('T',nT),('C',nC),('G',nG),('-',n6)]
	# Selection des lettres les plus courantes
	liste_sorted=sorted(liste, key=lambda nucleo: nucleo[1])
	liste_sorted.reverse()
	valeur=0
	word=''
	#print ('liste',liste_sorted)
	for i in range(0,4,1):
		valeur+=liste_sorted[i][1]
		word='%s%s' % (word,liste_sorted[i][0])
		if i ==4:
			break
		elif i<4 and valeur >= threshold and liste_sorted[i+1][1]!=liste_sorted[i][1]:
				break
	# Determination du mot consensus
	wordUngapped=''
	for lettre in word:
		if lettre!='-':
			wordUngapped='%s%s' % (wordUngapped,lettre)
	word=wordUngapped
	if len(word)==0:
		consensus='-'
	elif len(word)==1:
		consensus=word
	elif len(word)==2:
		if 'A' in word:
			if 'T' in word:
				consensus='W'
			if 'C' in word:
				consensus='M'
			if 'G' in word:
				consensus='R'
		if 'T' in word:
			 if 'C' in word:
			 	consensus='Y'
			 if 'G' in word:
			 	consensus='K'
		elif 'C' in word and 'G' in word:
			consensus='S'
	elif len(word)==3:
		if 'A' not in word:
			consensus='B'
		if 'T' not in word:
			consensus='V'
		if 'C' not in word:
			consensus='D'
		if 'G' not in word:
			consensus='H'
	elif len(word)>=4:
		consensus='N'
	return consensus

#def consensusW(word,threshold = 0.9):
#	'''
#	Count the number of appearance of each nucleotide in word, if there is a nucleotide having a maximum count > threshold, return this nucleotide, else return a '?'. 
#	*##################################
#	'''
#	nucl_list =  ['A', 'T', 'C', 'G', '-']
#	nucl_counts = [word.count(letter) for letter in nucl_list]
#	somme = sum(nucl_counts)
#	if somme == 0:
#		return 'N'
#	nucl_counts = [count/float(somme) for count in nucl_counts]
#	max_count = max(nucl_counts)
#	try:	
#		max_index = nucl_counts.index(max_count)
#		if max_count >= threshold:
#			consensus_letter = nucl_list[max_index]
#		else:
#			consensus_letter = '?'
#	except:
#		consensus_letter = '?'
#	return consensus_letter

def glueNoGapQSeqs(list_of_hits, ref_seq):
	'''input a list of hits containing all the hits of one species to a ref contig'''
	
	def check_rc(nobj): #detect reverse-complement and correct it
		if nobj.rcSign:
			nobj.qSeq=bs.reverse_complement(nobj.qSeq)
			nobj.sSeq=bs.reverse_complement(nobj.sSeq)
			nobj.sStart, nobj.sEnd = nobj.sEnd, nobj.sStart
			nobj.rcSign=False
		return nobj
		
	def addMargins(obj, ref_length):
#		temp_seq = obj.qSeq
		left_margin = obj.sStart-1
		right_margin = ref_length - obj.sEnd
		nseq= left_margin * 'N' + obj.qSeq + right_margin * 'N'
		return nseq
		
	sp_name=list_of_hits[0].qName
	nlst=copy.deepcopy(list_of_hits)
	nlst=[check_rc(item) for item in nlst]
	ref_length=len(ref_seq)
	qSeq_list=[addMargins(item, ref_length) for item in nlst]
	consensus_seq = map(lambda index: consensusW([qSeq[index] for qSeq in qSeq_list], consensus_threshold), range(0,ref_length))
	consensus_seq = ''.join(consensus_seq)
	return consensus_seq

def findGaps(seq):
	seq='>' + seq
	gap_count = 0
	gap_list = []
	for i in range(1,len(seq)):
		if seq[i] != '-':
			gap_list.append(gap_count)
			gap_count = 0
			continue
		gap_count += 1
	return gap_list[1:] + [0]
	
def combineGaps(gap_counts, sum_gap_counts, sStart, sEnd):
	def combine(i):
		if i < sStart or i > sEnd - 1:
			return sum_gap_counts[i]
		return max(gap_counts[i - sStart], sum_gap_counts[i])
		 
	return [combine(i) for i in range(1,len(sum_gap_counts))] + [0]
	
def alignGaps(seq, gap_list, start, end):
	pure_seq = seq.replace('-', '')
	gaps = gap_list[start-1:]
	res_seq = ''
	for i in range(0,len(pure_seq)):
		if gaps[i]==0:
			res_seq += pure_seq[i]
		else:
			res_seq += pure_seq[i] + gaps[i] * '-'
	return res_seq
	
def alignGaps_num(ind_list, gap_list, start, end):
	try:
		pure_inds = ind_list.remove(-1) #-1 represents a gap
	except:
		pure_inds = ind_list
	
	gaps = gap_list[start-1:]
	res_inds = []
	for i in range(0,len(pure_inds)):
		if gaps[i]==0:
			res_inds = res_inds + [pure_inds[i]]
		else:
			res_inds = res_inds + [pure_inds[i]] + gaps[i] * [-1]
	return res_inds
			


def makeQIndices(obj):
	pt = obj.sStart
	qSeq_length = len(obj.qSeq)
	inds = qSeq_length * [-1]
	qSeq = obj.qSeq
	sSeq = obj.sSeq
	for i in range(0, qSeq_length):
		if qSeq[i] != '-' and sSeq[i] != '-':
			inds[i] = pt
			pt += 1
		elif sSeq[i] != '-':
			pt += 1
		else:
			continue
	return inds

def alignQSeq(qobj, ref_ind):
	q_ind = makeQIndices(qobj)
	qseq = qobj.qSeq
	seq_length = len(ref_ind)
	qStart = q_ind[0]                            #positive value assumed
	left_margin = ref_ind.index(qStart)
	res_seq = (left_margin) * 'N'
	q_length = len(q_ind)
	q_pt = 0
	ref_pt = 0
	qind_pt = 0
	while q_pt < q_length:
		if q_ind[qind_pt] == ref_ind[ref_pt + left_margin] or q_ind[qind_pt] == -1:
			res_seq += qseq[q_pt]
			q_pt += 1
			ref_pt += 1
			qind_pt += 1
		else:        
			res_seq += '-'
			ref_pt += 1
	res_seq += (seq_length - len(res_seq)) * 'N'
	return [qobj.qName, res_seq]

def alignAllSeq(list_of_hits, ref_seq):
	def detectRange(pos,se_list):
		signs = [pos<se_list[0][i] or pos>se_list[1][i] for i in range(0, len(se_list[0]))]
		return signs
#	gap_count = [-1] + len(ref_seq) * [0]
#	sSeq_list = [obj.sSeq for obj in list_of_hits]
	sSeq_name = list_of_hits[0].sName
	ref_length = len(ref_seq)
	ref_ind = range(1, ref_length+1)
#	sStart_end_list = [[obj.sStart for obj in list_of_hits], [obj.sEnd for obj in list_of_hits]]
	gap_count_list = [combineGaps(findGaps(obj.sSeq), [0] * ref_length, obj.sStart, obj.sEnd) for obj in list_of_hits]
	gap_list = [max([lst[i] for lst in gap_count_list]) for i in range(0, ref_length)]
	ref_seq_gapped = alignGaps(ref_seq, gap_list, 1, ref_length)
	ref_ind_gapped = alignGaps_num(ref_ind, gap_list, 1, ref_length)
	qSeq_gapped = [alignQSeq(item, ref_ind_gapped) for item in list_of_hits]
	return [[sSeq_name, ref_seq_gapped]] + qSeq_gapped

def integrateQSeqsByRef(list_of_hits, ref_seq, sp_names):
	'''input a list of hits containing all the hits of one species to a ref contig'''
	
	def check_rc(nobj): #detect reverse-complement and correct it
		if nobj.rcSign:
			nobj.qSeq=bs.reverse_complement(nobj.qSeq)
			nobj.sSeq=bs.reverse_complement(nobj.sSeq)
			nobj.sStart, nobj.sEnd = nobj.sEnd, nobj.sStart
#			nobj.qStart, nobj.qEnd = nobj.qEnd, nobj.qStart
			nobj.rcSign=False
		return nobj
		
#	sp_name = list_of_hits[0].qName[:name_length]
#	ref_name = list_of_hits[0].sName
#	try:
#		sp_names = sp_names
#	except:
#		sp_names = getqSpeciesNames(list_of_hits)
	
	nlst = copy.deepcopy(list_of_hits)
	nlst = [check_rc(item) for item in nlst]
	align_res = alignAllSeq(nlst, ref_seq)
	qSeq_list = align_res[1:]
	res_ref_seq = align_res[0][1]
	ref_length = len(res_ref_seq)
	aligned_qseqs_by_sp = [[name, [lst[1] for lst in qSeq_list if lst[0][:name_length] == name]] for name in sp_names]
	
	consensus_seq_list = []
	for seq_of_sp in aligned_qseqs_by_sp:
		qSeq_list = seq_of_sp[1]
		consensus_seq = map(lambda index: consensusW([qSeq[index] for qSeq in qSeq_list], consensus_threshold), range(0,ref_length))
		consensus_seq = ''.join(consensus_seq)
		consensus_seq_list.append([seq_of_sp[0], consensus_seq])
	#the return is like [[Ref_name, ref_seq], [sp1, consensus_seq1], [sp2, consensus_seq2],...]
	return [align_res[0]] + consensus_seq_list
	

def integrate(list_of_hits, dict_ref_seq):
	''''''
#	try:
#	dict_ref_seq = ref_dict
#	except:
#		ref_path = 'ref_Esem.fasta'
#		dict_ref_seq = loadRef(ref_path)
#	catch_ref_length = re.compile('length_\d*')
	ref_seq_list = getRefSeqNames(list_of_hits)
	sp_names = sorted(getqSpeciesNames(list_of_hits))
#	try:
#		pool = Pool(thread_num)
#	except:
#		pool = Pool(2)
#	res = [integrateQSeqsByRef(findHitsByRefSeq(ref_name, list_of_hits)[1], dict_ref_seq[ref_name], sp_names) for ref_name in ref_seq_list]
	res = map(lambda ref_name: integrateQSeqsByRef(findHitsByRefSeq(ref_name, list_of_hits)[1], dict_ref_seq[ref_name], sp_names), ref_seq_list)
#	res = []
#	count = 0
#	ref_size = len(ref_seq_list)
#	for ref_name in ref_seq_list:
#		res.append(integrateQSeqsByRef(findHitsByRefSeq(ref_name, list_of_hits)[1], dict_ref_seq[ref_name], sp_names))
#		count += 1
#		if count % 100 == 0:
#			print '%s out of %s ref sequences have been aligned to queries' %(count, ref_size)
#	pool.close()
#	pool.join()
	return res

###statistical functions
def countATCG(res):
	'''input a set of res of one ref sequence from integrate, return the proportions of ATCGs in each sequence'''
	def countNucl(seq):
		return seq.count('A') + seq.count('T') + seq.count('C') + seq.count('G')
#	seq_length = float(len(res[0][1]))
	counts = [countNucl(item[1]) for item in res]
	return counts

###Output functions
	
def concatAligned(aligned_res, list_of_hits):
	'''input a result list from integrate, concatenate all the aligned contigs of each species, including the ref'''
	sp_list = getqSpeciesNames(list_of_hits)
	sp_num = len(sp_list)
	seq_list_by_sp = ((aligned_res[i][j][1] for i in range(0,len(aligned_res))) for j in range(0, sp_num))
	return (''.join(lst) for lst in seq_list_by_sp)
	
def tmp_write_res(res, path):
	handle=open(path, 'w')
	for i in range(0,len(res)):
		for j in range(0, len(res[i])):
			handle.write('>' + res[i][j][0] + '\n')
			handle.write(res[i][j][1] + '\n')
	handle.close()
	return False
	
#def write_res(res, list_of_hits):
	
	
def write_concat_results(res, output_path):
	'''receive a list of aligned results from integrate, write a concatenated sequence in one line for each species'''
	catch_ref_length = re.compile('length_+\d+')
	catch_len_num = re.compile('\d+')
	###sort the results by ref name
	try:
		res = sorted(res, key=lambda item: int(catch_len_num.findall(catch_ref_length.findall(item[0][0])[0])[0]))
	except:
		catch_ref_length = re.compile('\d+\.')
		catch_len_num = re.compile('\d+')
		res = sorted(res, key=lambda item: int(catch_len_num.findall(catch_ref_length.findall(item[0][0])[0])[0]))
	sp_names = sorted([item[0] for item in res[0][1:]])  ##item is like [[ref_name, ref_seq], [sp1_name, sp1_seq],...]
	handle = open(output_path + '_cThreshold_%s.out'%consensus_threshold, 'w')
	handle.write('>' + res[0][0][0][:name_length] + '_ref\n')
	ref_line = ''.join([item[0][1] for item in res])
	handle.write(ref_line + '\n')
	for name in sp_names:
		handle.write('>' + name + '\n')
#		line = [[item[j][1] for item in res if item[j][0] == name] for j in range(1,len(res[0]))].remove([])
		line = []
		for item in res:
			for pair in item:
				if pair[0] == name:
					line.extend(pair[1])
		line = ''.join(line)
		handle.write(line + '\n')
	handle.close()
	return False


###sequence filtering by list

def readList(path):
	'''read a list of contig ids to be filtered'''
	handle = open(path, 'r')
	lines = handle.readlines()
	handle.close()
	return [line[:-1] for line in lines]

def deleteSeqs(seq_dict, seq_list):
	'''delete seqs in a dict'''
	for seq_id in seq_list:
		try:
			seq_dict.pop(seq_id)
		except:
			0
	return seq_dict

def writeSeqFasta(seq_dict, path):
	handle = open(path, 'w')
	for item in seq_dict:
		handle.write('>' + item + '\n')
		handle.write(seq_dict[item] + '\n')
	handle.close()
	return False
	
	