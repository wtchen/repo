#! /usr/bin/python
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 04 17:18:09 2015

@author: CHEN
"""
import sys

def readBlast(path):
	handle = open(path, 'r')
	lines = handle.readlines()
	handle.close()
	db = [line.split('\t') for line in lines]
	return db
	
def parseDB(db):
	sp_names =list(set([seqids[1][:6] for seqids in db]))
	list_of_remove = [list(set([item[0] + '\n' for item in db if item[0][:6] == name and item[0] != item[1]])) for name in sp_names]
	return list_of_remove
	
def writeList(lst, path):
	handle = open(path, 'w')
	for item in lst:
		handle.writelines(item)
	handle.close()
	return False
	
in_path = sys.argv[1]
out_path = sys.argv[2]

db = readBlast(in_path)
lst = parseDB(db)
writeList(lst, out_path)