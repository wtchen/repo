#! /usr/bin/python
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 23 11:08:48 2015
multiprocessing version
command:
	./run_pc.py blast_file_path ref_path consensus_threshold out_prefix number_of_threads trunk_size qSeq_path
@author: CHEN
"""
def pickable(ref_name):
	return pc.integrateQSeqsByRef(pc.findHitsByRefSeq(ref_name, blast_data)[1], dict_ref_seq[ref_name], sp_names)

import parse_and_concat_inv_keep as pc
import sys
from multiprocessing import Pool

try:
	pc.qSeq_path = sys.argv[7]
	if pc.qSeq_path != '':
		pc.notDNA = True
except:
	False

blast_data = pc.storeInfos(pc.loadBlast(sys.argv[1]))
dict_ref_seq = pc.loadRef(sys.argv[2])
sp_names = pc.getqSpeciesNames(blast_data)
print '%s blasts on %s reference contigs found\n'%(len(blast_data), len(pc.getRefSeqNames(blast_data)))
print 'Now parsing blast data\n'
pc.consensus_threshold = float(sys.argv[3])
consensus_threshold = float(sys.argv[3])
#parse_results = pc.integrate(blast_data, pc.ref_seq_list)
ref_seq_list = pc.getRefSeqNames(blast_data)
sp_names = sorted(pc.getqSpeciesNames(blast_data))

try:
	thread_num = int(sys.argv[5])
except:
	thread_num = 20
try:
	trunk_size = int(sys.argv[6])
except:
	trunk_size = 50

pool = Pool(thread_num)
parse_results = pool.map(pickable, ref_seq_list, trunk_size)
pool.close()
pool.join()
print 'parse complete, writing output...'
#pc.tmp_write_res(parse_results, 'test_parse.txt')
pc.write_concat_results(parse_results, sys.argv[4])
print 'Job done!'