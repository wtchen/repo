#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 10 10:54:27 2015
Filter an aligned set of sequences, delete sites with too few unambiguous nucleotides
@author: CHEN
"""
import sys

#####################
def loadSeq_db(path):
	'''load a fasta file containing all the contigs of multiple (or single) species and parse it, return a list'''
	handle=open(path,'r')
	lines=handle.readlines()
	handle.close()
	seq_list = []
	line = ''
	key = 'null'
	lines_size = len(lines)
	for i in range(0, lines_size):
		if lines[i][0] == '>' or lines[i] == '':
			seq_list.append([key, line])
			key = lines[i][1:-1]
			key = key.replace(' ', '')
			line = ''
			continue
		else:
			line += lines[i][:-1]
	seq_list.append([key, line])
	return seq_list[1:]

def filterSeqs(seq_db, min_nucls):
	def countNucls(word_list):
		return sum([word in set(['A', 'T', 'C', 'G', 'a', 't', 'c', 'g']) for word in word_list])
	
	seq_length = len(seq_db[0][1])
	seq_db_lst = [[item[0], list(item[1])] for item in seq_db]
	for i in range(0, seq_length):
		word_list = [item[1][i] for item in seq_db_lst]
		if countNucls(word_list) < min_nucls:
			map(lambda item: item[1].pop(i), seq_db_lst)
	seq_db = [[item[0], ''.join(item[1])] for item in seq_db_lst]
#	for i in range(0, seq_length):
#		word_list = [item[1][i] for item in seq_db]
#		if countNucls(word_list) < min_nucls:
#			for j in range(0, len(seq_db)):
#				seq_db[j][1] = seq_db[j][1][:i] + seq_db[j][1][j+1:]
	return seq_db

def writeSeq_db(seq_db, filename):
	handle = open(filename, 'w')
	for contig in seq_db:
		handle.write('>' + contig[0] + '\n')
		handle.write(contig[1] + '\n')
	handle.close()
	return False
######################	
seq_fn = sys.argv[1]
min_nucls = int(sys.argv[2])
output_fn = sys.argv[3]

print 'Loading fasta...'
seq_db = loadSeq_db(seq_fn)
print 'Filtering sequences...'
res = filterSeqs(seq_db, min_nucls)
print 'Writing filtered sequences...'
writeSeq_db(res, output_fn)
print 'Done.'