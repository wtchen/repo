#! /usr/bin/python
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 30 15:19:03 2015
Usage:
./pick_seq_by_list.py dataset_path list_path outpath
@author: CHEN
"""
import sys

def readData(path):
	handle = open(path, 'r')
	lines = handle.readlines()
	handle.close()
	res = []
	tmp_seq = []
	title = ''
	for line in lines:
		if line[0] != '>':
			tmp_seq = tmp_seq + line[:-1]
		else:
			res.append([title, tmp_seq])
			title = line.split(' ')[0][:-1]
			tmp_seq = ''
	res.append([title, tmp_seq])
	return res[1:]
	
def loadList(path):
	handle = open(path, 'r')
	lines = handle.readlines()
	handle.close()
	lst = []
	for line in lines:
		lst.append(line[:-1])
	return(lst)

def filterData(data, lst):
	res = [[item for item in data if item[0][1:] == name] for name in lst]
	return res
	
def writeRes(res, path):
	handle=open(path, 'w')
	for item in res:
		for group in item:
			for term in group:
				handle.write(term + '\n')
	handle.close()
	return False

dataset_path = sys.argv[1]
list_path = sys.argv[2]
out_path = sys.argv[3]

data = readData(dataset_path)
lst = loadList(list_path)
res = filterData(data, lst)
writeRes(res, out_path)

