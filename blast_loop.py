#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 09 14:00:21 2015
usage:
	./blast_loop.py fasta_filename blast_order_list_filename number_of_threads_to_use_in_blastn
@author: CHEN
"""
import os
import sys
import gc


def loadSeq_db(path):
	'''load a fasta file containing all the contigs of multiple (or single) species and parse it, return a list'''
	handle=open(path,'r')
	lines=handle.readlines()
	handle.close()
	seq_list = []
	line = ''
	key = 'null'
	lines_size = len(lines)
	for i in range(0, lines_size):
		if lines[i][0] == '>' or lines[i] == '':
			seq_list.append([key, line])
			key = lines[i][1:-1]
			key = key.replace(' ', '')
			line = ''
			continue
		else:
			line += lines[i][:-1]
	seq_list.append([key, line])
	return seq_list[1:]
	
#def splitSeqList(seq_list):
#	sp_names = list(set([item[0][:6] for item in seq_list]))
#	print sp_names
#	res = [[item for item in seq_list if item[0][:6] == name] for name in sp_names]
#	return res

def writeSeq_db(seq_db, filename):
	handle = open(filename, 'w')
	for contig in seq_db:
		handle.write('>' + contig[0] + '\n')
		handle.write(contig[1] + '\n')
	handle.close()
	return False

def loadList(path):
	handle = open(path, 'r')
	lines = handle.readlines()
	handle.close()
	lst = []
	for line in lines:
		lst.append(line[:-1])
	return(lst)

def writeSplitedList(sp, prefix):
	handle = open(prefix + 'blast.tmp', 'w')
	for i in range(0, len(sp)):
		handle.write('>' + sp[i][0] + '\n')
		handle.write(sp[i][1] + '\n')
	handle.close()
	return False
	
def writeList(lst, path):
	handle = open(path, 'w')
	for item in lst:
		handle.writelines(item)
	handle.close()
	return False

def readFastaSeqNames(path):
	'''find all contig names in a fasta'''
	handle = open(path, 'r')
	lines = handle.readlines()
	seq_list = []
	for line in lines:
		if line[0] == '>':
			seq_list.append(line[1:-1])
	return seq_list

def filterData(data, lst):
	res = [[item for item in data if item[0] == name] for name in lst]
	return res

def writeFiltered(res, path):
	handle=open(path, 'w')
	for item in res:
		for group in item:
			for term in group:
				handle.write(term + '\n')
	handle.close()
	return False
	
def readMatched_from_blast(blast_fn):
	handle = open(blast_fn, 'r')
	lines = handle.readlines()
	matched_lst = set([line.split('\t')[5] for line in lines]) #qseqid is the 6th item of each line
	return matched_lst


def removeMatched(seq_db, list_of_matched):
	'''list of matched shall be a set'''
#	contig_set = set([item[0] for item in seq_db])
#	remain_lst = list(contig_set.difference(list_of_matched))
#	res_db = []
#	for contig_name in remain_lst:
#		res_db.extend([item for item in seq_db if item[0] == contig_name])
	seq_dict = {item[0]:item[1] for item in seq_db}
	for contig in list_of_matched:
		try:
			seq_dict.pop(contig)
		except:
			False
	res_db = [[key, seq_dict[key]] for key in seq_dict]
	return res_db
	
def main(fasta_filename, blast_order_filename):
	''''''
	seq_db = loadSeq_db(fasta_filename)
	blast_lst = loadList(blast_order_filename)
	subject_fn_suffix = '_subj.fasta'
	count = 0
	for current_subject_sp in blast_lst:
		if seq_db == []:
			break
		count += 1
		print 'Loop No.%d. Current subject species is %s'%(count, current_subject_sp)
		query_fn = 'query.fasta'
		subject_contigs = [item for item in seq_db if item[0][:6] == current_subject_sp]
		query_contigs = [item for item in seq_db if item[0][:6] != current_subject_sp]
		if query_contigs == []:
			break
		writeSeq_db(subject_contigs, current_subject_sp + subject_fn_suffix)
		writeSeq_db(seq_db, query_fn)
		os.system('makeblastdb -in ' + current_subject_sp + subject_fn_suffix + ' -dbtype nucl')
		print 'blasting current file...'
		blast_fn = 'no_' + str(count) + '_' + current_subject_sp + '_blast.out'
		os.system('blastn -db ' + current_subject_sp + subject_fn_suffix + ' -query ' + query_fn + ' -out ' + blast_fn + ' -outfmt "6 evalue qstart qend sstart send qseqid sseqid qseq sseq"' + '  -num_threads ' + blast_num_threads)
		matched_lst = readMatched_from_blast(blast_fn)
		print 'removing matched contigs from the database...'
		seq_db = removeMatched(query_contigs, matched_lst)
#	os.system('rm ' + '*' + subject_fn_suffix + '*')
	return False
		

###################################

seq_db_fn = sys.argv[1]

blast_order_fn = sys.argv[2]

try:
	blast_num_threads = int(sys.argv[3])
except:
	blast_num_threads = 1
main(seq_db_fn, blast_order_fn)























