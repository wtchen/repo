#! /usr/bin/python
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 04 14:39:09 2015

@author: CHEN
"""

import sys
#import parse_and_concat_inv_keep as pc
import os
#import commands as cd
from multiprocessing import Pool

def loadSeq(path):
	'''load the reference fasta and parse it, return a list'''
	handle=open(path,'r')
	lines=handle.readlines()
	handle.close()
	seq_list = []
	line = ''
	key = 'null'
	lines_size = len(lines)
	for i in range(0, lines_size):
		if lines[i][0] == '>' or lines[i] == '':
			seq_list.append([key, line])
			key = lines[i][1:-1]
			key = key.replace(' ', '')
			line = ''
			continue
		else:
			line += lines[i][:-1]
	seq_list.append([key, line])
	return seq_list[1:]
	
def splitSeqList(seq_list):
	sp_names = list(set([item[0][:6] for item in seq_list]))
	print sp_names
	res = [[item for item in seq_list if item[0][:6] == name] for name in sp_names]
	return res

def writeSplitedList(sp, prefix):
	handle = open(prefix + 'blast.tmp', 'w')
	for i in range(0, len(sp)):
		handle.write('>' + sp[i][0] + '\n')
		handle.write(sp[i][1] + '\n')
	handle.close()
	return False

def readBlast(path):
	handle = open(path, 'r')
	lines = handle.readlines()
	handle.close()
	db = [line.split('\t') for line in lines]
	return db
	
def parseDB(db):
	sp_names =list(set([seqids[1][:6] for seqids in db]))
	list_of_remove = [list(set([item[0] + '\n' for item in db if item[0][:6] == name and item[0] != item[1]])) for name in sp_names]
	return list_of_remove
	
def writeList(lst, path):
	handle = open(path, 'w')
	for item in lst:
		handle.writelines(item)
	handle.close()
	return False

def readFastaSeqNames(path):
	handle = open(path, 'r')
	lines = handle.readlines()
	seq_list = []
	for line in lines:
		if line[0] == '>':
			seq_list.append(line[1:-1])
	return seq_list

def filterData(data, lst):
	res = [[item for item in data if item[0] == name] for name in lst]
	return res

def writeFiltered(res, path):
	handle=open(path, 'w')
	for item in res:
		for group in item:
			for term in group:
				handle.write(term + '\n')
	handle.close()
	return False

def pickandblast(sp):
	sp_name = sp[0][0][:6]
	os.system('mkdir ./' + sp_name)
	path = './' + sp_name + '/'
	writeSplitedList(sp, path)
	os.system('makeblastdb -in ' +  path + 'blast.tmp ' + '-dbtype nucl')
	print 'blasting current file...'
	os.system('blastn -db ' + path + 'blast.tmp ' + '-query ' + path + 'blast.tmp ' + '-out ' + sp_name + '_blast.out ' + '-outfmt "6 qseqid sseqid evalue"')
	blast = readBlast(sp_name + '_blast.out')
	list_of_remove = parseDB(blast)
	seq_names_in_fasta = set(readFastaSeqNames(path + 'blast.tmp'))
	list_of_keep = seq_names_in_fasta - list_of_remove
	original_fasta = loadSeq(path + 'blast.tmp')
	filtered_fasta = filterData(original_fasta, list_of_keep)
	writeFiltered(filtered_fasta, sp_name + '_filterd.fasta')
	return False
	

	
	

fasta_path = sys.argv[1]
try:
	thread_num = int(sys.argv[2])
except:
	thread_num = 10
print 'reading data...'
seq_list = loadSeq(fasta_path)

print 'blast each species\' sequences...'
splited = splitSeqList(seq_list)
#print splited
#pool = Pool(thread_num)
#pool.map(pickandblast, splited)
#pool.close()
#pool.join()
i = 0
for sp in splited:
	print '%d blasted'%i
	pickandblast(sp)
	i += 1
print 'Job done!'

